#!/bin/bash

set -e

# TODO: these could be read from configuration.ccl
DISTBASE=boost
DISTVERSION=1_55_0
DISTNAME=${DISTBASE}_${DISTVERSION}.tar.gz
DISTHASH=93780777cfbf999a600f62883bd54b17
DISTSRC=https://bitbucket.org/ianhinder/boost/downloads

function check_md5()
{
    hash=$1
    file=$2

    if md5sum --help >/dev/null 2>&1; then
        md5sum --quiet -c - <<EOF
$DISTHASH  $LOCALDISTFILETMP
EOF
    elif [ $(echo 1 | md5 2>/dev/null) = b026324c6904b2a9cb4b88d6d61c81d1 ]; then
        local output=( $(md5 -r $file) )
        if [ "${output[0]}" != $hash ]; then
            echo "File $file does not match checksum $hash" >&2
            return 1
        fi
        # else
        # If there is no md5 checking utility present, don't do the check
    fi
}

# TODO: this could be done by a Cactus library function
LIBCACHE=${CCTK_HOME:-$PWD}/libcache
LOCALDISTFILE=$LIBCACHE/$DISTNAME
LOCALDISTFILETMP=${LOCALDISTFILE}.tmp
if [ ! -r "${LOCALDISTFILE}" ]; then
    URL=$DISTSRC/$DISTNAME
    mkdir -p $LIBCACHE
    # TODO: allow location of curl to come from Cactus option
    if ! CURLOUTPUT="$(curl -L -f -o $LOCALDISTFILETMP $URL 2>&1)"; then
        echo "Error while downloading $URL:"
        echo "$CURLOUTPUT"
        exit 1
    fi

    #    echo 1 >> $LOCALDISTFILETMP
    check_md5 $DISTHASH $LOCALDISTFILETMP

    rm -f $LIBCACHE/${DISTBASE}_*.tar.gz
    mv $LOCALDISTFILETMP $LOCALDISTFILE
fi



echo ${LOCALDISTFILE}
